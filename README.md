# FASTENV

## 概要

静的なサイトを制作するための環境を高速につくります。
ベストプラクティスが凝縮されたフォルダ構成、命名を徹底します。

### ディレクトリ構成

- template.html: HTMLの雛形
- resources: 画像、フォント、CSS、javascript、外部ライブラリ etc.
	- images: 画像
	- fonts: フォント
	- styles: CSS
		- Mixins: 実装単位で使い回すMixinのライブラリ。＝＞Pages内のファイルがimportして使用する。
		- Componets: セレクター単位で使い回すCSS部品(コンポーネント)のライブラリ。＝＞Pages内のファイルがimportして使用する。
		- Utility: 便利なページを横断して使える共有スタイル。＝＞Pages内のファイルがimportして使用する。
		- Foundations: 初期設定や環境設定系のファイル群。
			- reset.css: リセットCSS
			- default.css: デフォルトスタイルシート
			- const.css: 色やサイズなどの定数を定義するファイル
		- Pages: ページごとのCSS。 <= エントリーポイント
	- scripts: javascript
	- vendor: 外部ライブラリ
		- bootstrap-4.2.1
		- jquery
			- jquery-3.3.1.min.js